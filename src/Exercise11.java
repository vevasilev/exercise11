import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Exercise11 {
    public static void main(String[] args) {
        int numberWords = 10;
        System.out.println("Enter a string:");
        String sourceString = new Scanner(System.in, StandardCharsets.UTF_8).nextLine();
        System.out.println("The " + numberWords + " most common words in a string:");
        Arrays.stream(sourceString.split("[\\p{Punct}\\s]+"))
                .map(String::toLowerCase)
                .collect(Collectors.groupingBy(word -> word, Collectors.counting()))
                .entrySet()
                .stream()
                .sorted((word1, word2) ->
                        word1.getValue().equals(word2.getValue())
                                ? word1.getKey().compareTo(word2.getKey())
                                : word2.getValue().compareTo(word1.getValue())
                )
                .limit(numberWords)
                .forEach(e -> System.out.println(e.getKey()));
    }
}